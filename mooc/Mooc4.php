<?php 
		require_once("connexio.php");
		require_once("MoocConnexion.php");
	$req = $db->prepare("SELECT * FROM mooc_avenir WHERE id_cour = :idcour");
	$req->bindParam(':idcour',$_POST['id_cour'] );
	$req->execute();
	 while($donnees=$req->fetch()){
        $nom=$donnees["nom"];
		$prenom=$donnees["prenom"];
		$email=$donnees["email"];
		$nom_cour=$donnees["nom_cour"];
		$date=$donnees["date"];
		$heure=$donnees["heure"];
		$description=$donnees["description"];
		$lienVideo=$donnees["lienvideo"];
		$id_categorie=$donnees["id_categorie"];
		
    } ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title> Mooc</title>
		<link rel="stylesheet" href="nobel.css"/>
	</head>
	<body>
		<header>
			<h1><a href=""> mooc </a></h1>
		</header>

		<main>
		<p><center><strong><?php echo $nom_cour ?></strong></center></p>
		<h2>À propos du cours</h2><?php echo $lienVideo ?>
  <p><?php echo $description?></p>
		
		
	    <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
    <div id="player"></div>

    <script>
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '360',
          width: '640',
          videoId: '5hjUnHjZ56g',
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
      }

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
          setTimeout(stopVideo, 6000);
          done = true;
        }
      }
      function stopVideo() {
        player.stopVideo();
      }
    </script>
		<p>si vous voulez visualiser d'autre mooc veuillez <a href="Mooc1.php">cliquer ici</a></p>
				
		</main>
	</body>
</html>