-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 10 jan. 2018 à 22:07
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mooc`
--

-- --------------------------------------------------------

--
-- Structure de la table `mooc_avenir`
--

DROP TABLE IF EXISTS `mooc_avenir`;
CREATE TABLE IF NOT EXISTS `mooc_avenir` (
  `id_cour` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `email` text NOT NULL,
  `nom_cour` text NOT NULL,
  `date` date NOT NULL,
  `heure` time NOT NULL,
  `description` longtext NOT NULL,
  `lienvideo` text NOT NULL,
  `id_categorie` int(11) NOT NULL,
  PRIMARY KEY (`id_cour`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `mooc_avenir`
--

INSERT INTO `mooc_avenir` (`id_cour`, `nom`, `prenom`, `email`, `nom_cour`, `date`, `heure`, `description`, `lienvideo`, `id_categorie`) VALUES
(1, 'ahmed', 'mansouri', 'mansouri.ahmed14@gmail.com', 'lol', '2016-11-14', '14:11:00', 'trop bien', 'https://www.youtube.com/watch?v=TjgTkwjdDiE', 3),
(2, 'zitoune', 'achraf', 'mo@gmail.com', 'Le cour de l\'année', '2015-11-14', '14:00:00', 'Ce cour est tellement facile que vous alez reussir votre annee', 'https://www.youtube.com/watch?v=TjgTkwjdDiE', 6),
(3, 'rayane', 'benma', 'rayane@oklmair.com', 'By the best', '2018-02-22', '12:00:00', 'Ce cour sera genre mega facil il me permettra de vous presentez mes qualité et comment avoir des bonne note trop facilement c vraiment ouf vous verrez.#teamKenzaFarah', 'https://www.youtube.com/watch?v=NtJ_IezDiDs', 6);

-- --------------------------------------------------------

--
-- Structure de la table `m_categorie`
--

DROP TABLE IF EXISTS `m_categorie`;
CREATE TABLE IF NOT EXISTS `m_categorie` (
  `id_mcategorie` int(11) NOT NULL AUTO_INCREMENT,
  `Category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_mcategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `m_categorie`
--

INSERT INTO `m_categorie` (`id_mcategorie`, `Category`) VALUES
(3, 'Energie Renouvelable'),
(4, 'AMÃ‰LIORATION CONTINUE DES EXPLOITATIONS AGRICOLES'),
(6, 'MACHINISME AGRICOLE');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
